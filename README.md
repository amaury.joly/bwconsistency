# Consistence Faible Byzantine Pour le Cloud

Ce projet est hébergé ici: [https://amauryjoly.fr/gitea/amaury_joly/bwconsistency](https://amauryjoly.fr/gitea/amaury_joly/bwconsistency).  
Un miror est disponible sur le gitlab du laboratoire du LIS: [https://gitlab.lis-lab.fr/amaury.joly/bwconsistency](https://gitlab.lis-lab.fr/amaury.joly/bwconsistency)

## Introduction

Ce dépot compile mes recherches et travaux autour du sujet de la Consistence Faible Byzantine Pour le Cloud. (cf. [sujet de stage](./bwconsistency-stage.pdf))

## Membres

Ce projet est réalisé par Amaury JOLY, et encadré par Emmanuel GODARD et Corentin TRAVERS. Ainsi que dans une collaboration étroite avec l'entreprise [Scille](https://scille.eu/).

## Architecture

Le dossier `./recherches` contient les resumés des différents documents que j'ai pu consulter durant mes recherches.