# Practical Client-side Replication: Weak Consistency Semantics for Insecure Settings

## Authors: van der Linde, Leitao, Preguica

## Résumé:

Le papier spécifie une amélioration de la cohérence causale, rajoutant des propriétés en renforçant la sécurité. Ils comparent ensuite différentes implémentations de leurs solutions en axant sur le besoin d'une faible latence pour privilégier l'interactivité.

## Plan:

1. Présente les attaques possibles sur la cohérence causale. $3
2. Définissent les propriétés d'une cohérence causale sécurisée répondant aux attaques. $4
3. Définit de nouvelles classes de cohérence étendant la cohérence causale. $5
4. Définit des algorithmes pour implémenter ces classes de cohérence. $5
5. Présente des résultats de performance de ces algorithmes. $6

## Détails du document

### Types d'attaques

- Tempering: un nœud soumet une opération pour anticiper une opération en attente qui n'a pas encore été exécutée par le système.
- Omitting dependencies: un nœud n'utilise qu'un sous-ensemble des opérations dans la dépendance. Il sera en mesure de soumettre une tâche concurrente au système.
- Unseen dependencies (également appelé add): un nœud soumet une opération qui dépend d'une opération qu'il n'a pas vue. Il permet à l'attaquant d'anticiper une opération. (C'est différent du tempering car dans ce cas l'opération n'existe pas encore).
- Combining omitting and unseen: un nœud peut omettre une dépendance et soumettre une opération qui dépend d'une opération qu'il n'a pas vue.
- Sibbling generation: créer deux opérations différentes avec le même id. L'attaquant pourrait créer une divergence permanente entre les nœuds.

### Propriétés d'une cohérence causale sécurisée

- **Immutable History**: Chaque opération est envoyée avec son passé causal à chaque nœud valide. (Contrecarre le tempering)
- **No Future Dependencies**: Chaque opération est envoyée avec son état de connaissance de l'état des nœuds du système. (Contrecarre l'unseen dependencies puisque l'opération sera considérée par l'ensemble du système comme "en retard" et sera donc ignorée)
- **Causal Execution**: Toute opération $o_i$ appartenant au passé causal d'une opération $o$ doit être sérialisable t.q. : $o_i < o$. (Force une sorte de synchronisation entre les nœuds)
- **Eventual Sibling Detection**: Chaque opération doit être considérée comme une faute éventuelle et doit donc avoir la possibilité d'être révoqué. La révocation ne peut se produire qu'après l'exécution de l'opération. (Assure que si deux opérations sont créées avec un même identifiant et crée une divergence, alors les nœuds auront toujours un moyen de retourner à un état convergent. Contrecarre **en partie** le sibling generation)
- **Limitted Omission**: 

<!-- OLD
# Practical Client-side Replication: Weak Consistency Semantics for Insecure Settings

## Authors: van der Linde, Leitao, Preguica

## Definition

causal consistency: model enforcing clients to observe a state that respects the causal order of operations. (this is the case for decentralized and peer to peer systems)

Attacks on causal consistency:

- Tempering: a node submit an operation to anticipate a pending operation actually not yet executed by the system.
- Omitting dependencies: a node used only a subset of the operations in the dependency. He will be able to submit a concurrent task to the system.
- Unseen dependencies (also called add): a node submit an operation that depends on an operation that he didn't see. It can be usefull for the attacker to anticipate the operation. (This is different from tempering because in this case the operation does not exist yet).
- Combining omitting and unseen: a node can omit a dependency and submit an operation that depends on an operation that he didn't see.
- Sibbling generation: creating two differents operations with the same id. The attacker could create a permanent state divergence between the nodes.

## Summary

### Solutions used in the paper

#### Secure Causal Consistency
Autors defined the properties of a secure causal consistency: Immutable History, No Future Dependencies, Causal Executions, Limitted Omission, and Eventual Sibling Detection.

The algorithms they propose used the following solutions for each property:

- Immutable History: The nodes sign the operations and the dependencies. The nodes can't temper the history because they can't sign the operation.
- No Future Dependencies: Each operations includes a hash of all direct causal dependencies. The nodes can't omit dependencies because they can't sign the operation.
- Causal Executions: The nodes need to verify, before executing an operation, that all the dependencies are executed.
- Limitted Omission: It's by design impossible due to the metadata (hash of the dependencies).
- Eventual Sibling Detection: Many mechanism are used:
  - a node is able to detect when two operations with the same id are send from differents paths.
  - a node is able than the hash of the dependencies is different with the hash provide by the operation.
  - the nodes are comparing the dependencies of the operation between them. If they are different, they are able to detect the sibbling generation.

#### Secure Strict Causal Consistency

The Secure Strict Causas Consistency is a variant of the Secure Causal Consistency who is using a trusted service. Such as the enclave in Intel SGX. Thus the usage of a hash of the dependencies is unnecessary.
An issue of this solution is the cost of the connection to the trusted service. A possible attack would be to connect and disconnect a lot of time of the trusted service to make the system slow.
This sollution was not explored in the paper due to this issue. -->
