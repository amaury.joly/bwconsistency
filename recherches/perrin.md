# Concurrence et cohérence dans les systèmes répartis

## Auteur: Matthieu Perrin

## Réflexions

Un peu de mal à comprendre les bornes de cohérence.
Ça veut dire quoi composable ?

## Définitions

Système réparti : Collection d'entités de calcul autonomes connectées en vue d'accomplir une tâche commune.

Entités de calcul : (ou processus). Entité d'un réseau capable de décision en fonction de stimuli.  

Cohérence forte : Les objets ciblés cachent la concurrence et se comportent comme si tous les accès était séquentiels.

## Introduction

Un système réparti est caractérisé par :

- L'échelle du système
- Les moyens d'interactions
- Gestion des fautes (c.f. : reynal18 attacks) (et nombre de fautes acceptables)
- Rapport au temps (y a-t-il une horloge partagée ?)

## Les histoires concurrentes

Une histoire concurrente est un ensemble d'événements partiellement ordonnés par un ordre de processus et étiquetés par des opérations.

3 primitives possibles :

- Broadcast (diffusion fiable) :
  - Validité : tout message reçu est émis par un processus
  - Uniformité : tout message reçu par un processus est reçu par tous les autres processus
- FIFO Broadcast (idem Broadcast) :
  - Réception FIFO : tout message reçu par un processus est reçu dans l'ordre d'émission
- Causal Broadcast (idem FIFO Broadcast) :
  - Réception causale : Tout message $m'$ envoyé par un processus après réception d'un message $m$ est aussi reçu après $m$ chez tous les autres processus

### Composabilité

La compossibilité définit la possibilité pour deux types de données abstraits différents, cohérente pris de manière unitaire, de pouvoir être combinés tout en gardant leurs cohérences.

### Décomposable

La décomposabilité définit la possibilité pour deux types de données abstraits différents cohérents si considérés "ensemble" de rester cohérent si considérés séparément.

### Localité

La localité est le respect simultané de la composabilité et de la décomposabilité.

## Modèles

Cohérence forte impossible dans des environnements crédibles de cloud. (Trop de risques de déni de services)

Ci-dessous une liste des différents paradigmes de modélisation de système répartis :

### Cohérence Séquentiel (Décomposable, Fort)

Cohérence Séquentiel (SC) : Les objets ciblés cachent la concurrence et se comportent comme si tous les accès était séquentiels.  
Le but est de mimer le comportement "comme si" un serveur centralisait et ordonnait l'information. (Ça peut être le cas ou non, il faut juste que la propriété soit respectée).  
Il y a un débat sur une notion de la cohérence séquentielle. La première formalisation de ce type de cohérence formulé par Lamport oublie de mentionner la notion de "synchronisation". Ce qui peut conduire a des comportements non cohérents. Elle permet par exemple l'existence d'histoires infinies qui viennent s'ajouter les unes derrières les autres. Ce qui serait absurde dans un système réel. (Exemple : infinité de lectures suivie d'une infinité d'écritures).  
Il y a donc débat sur la notion de cohérence séquentielle avec une école qui considère ce cas comme plausible et une autre qui souhaite rajouter une notion de synchronisation.

### Linéarisabilité ()

Il y a ici un lien fort entre l'ordre d'action du processus et son intégration au système. Il y a une synchronicité plus forte.  
Ici lorsqu'un processus souhaite accéder à un objet, s'il ne rentre pas en conflit avec aucune action d'écriture, il récupère la valeur antérieure à son exécution. (propriété : Registre Sûr).  
Si plusieurs processus veulent accéder à un objet, et entrent en concurrence avec une écriture, alors ils ne peuvent retourner seulement la valeur avant ou après l'écriture (propriété : Registre Régulier).  
Si deux lectures ne sont pas concurrentes, alors elles doivent retourner une valeur au moins aussi récente que la lecture antérieure. (propriété : Registre Atomique).

### Sérialisabilité (Décomposable, Faible)

ACID : Atomicité (une transaction est soit complètement acceptée soit complètement avortée), Cohérence (Une transaction exécutée dans un état correct emmène vers un état correct), Isolation (Les transactions n'interfèrent pas entre elles), Durabilité (une transaction acceptée n'est pas remise en cause).

La sérialisabilité est similaire à la linéarisabilité, à la différence que des transactions peuvent être avortés. Cela à pour effet de rendre le système moins "fort" en termes de consistance.

### Convergence (Composable, Faible)

La convergence est une notion de cohérence faible. Elle définit un système qui peut à un instant $t$ être divergent, mais qui finira sur un temps infini à converger vers un état commun.

### Convergence forte (Composable, Faible)

La convergence forte est une extension de la convergence où notre histoire est divisée en plusieurs états. Chaque transaction se trouve dans un état avec d'autres transactions avec qui elle partage un "passé commun". On définit le passé commun comme la base de connaissance antérieur à l'exécution de la transaction.

#### Data type pour la convergence

Les types de données vues pour les autres modèles sont peu adapté pour modéliser les interactions dans le cas de la convergence. On privilégie plutôt des types de données qui permettent de définir des états (ex : OR-SET).

### Intention

L'intention est une notion qui tend à appliquer la cohérence en fonction de l'intention des utilisateurs. Elle trouve son sens particulièrement dans l'édition collaborative lors d'écritures concurrentes. Mais sa spécification reste floue et c'est un concept qui semble difficile à appliquer.

### Cohérence pipeline (Décomposable, Faible)

La cohérence pipeline consiste une cohérence ne garantissant pas l'ordre des états finaux. C'est donc une cohérence faible. La chose la plus notable est que le résultat n'est pas garantit pour deux histoires concurrentes équivalentes.

### Cohérence de Cache (Composable, Décomposable, Fort)

On imagine que chaque type de donnée abstraite utilise une seule et même mémoire qu'il partage avec tous les processus de l'histoire concurrente. Chaque mémoire respecte une cohérence séquentielle.

### Cohérence d'écriture (Faible)

Un aspect manquant de la convergence est l'absence de cohérence d'écriture. C'est-à-dire que rien ne garantit que les données écrites par un processus soient bien celles lue à la fin par les lectures infinies.  
Le concept de cohérence d'écriture vise donc à spécifier cette propriété.

### Cohérence d'écriture forte (Faible)

La cohérence d'écriture forte est une extension de la cohérence d'écriture qui rajoute un ordre dans les opérations d'écriture. Ceci permet d'assurer que chaque opération soit faites dans le même état et assure donc une convergence plus "rapide".

## Cohérence causale

### Cohérence Causale Faible

Cohérence directe avec son passé local et respect de cette cohérence avec les autres processus par transitivité. Aucune préservation de l'ordre des opérations.
Résultat potentiellement divergent ?

### Convergence Causale

Rajout de la notion d'ordre totale. Qui permet de garantir la convergence du résultat.

### Cohérence Causale

Cohérence avec les écritures du passé causal et des lectures du passé local.
