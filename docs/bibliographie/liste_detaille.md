# Enumération de la bibliographie étudié
## Cohérence
### Très pertinents
__perrin_concurrence_2017__, "Concurrence et cohérence dans les systèmes répartis":  
Etat de l'art sur la cohérence dans les systèmes repartis. Présentation d'une approche de modélisation des histoires concurentes. Formaisations de différents critères de cohérences. Comparaison et "hierarchisation" des différents critères de cohérences.


### Intéressants mais redondants
__lamport_interprocess_1986__, "On interprocess communication":  
Formalisation d'une cohérence séquentiel "single writer"

__misra_axioms_1986__, "Axioms for memory access in asynchronous hardware systems":  
Exetnsion de lamport_interprocess_1986 dans une approche "multi-writer"

__lipton_pram_1988__, "{PRAM}: A Scalable Shared Memory":  
Definition de la mémoire PRAM (cohérence pipeline).

## Cohérence en contextes byzantins
### Algorithmes
__van_der_linde_practical_2020__, "Practical client-side replication: weak consistency semantics for insecure settings":  
Algorithme pour de la Cohérence causale BFT. (Reflexions sur des erreurs byzantines possible + algo et implé)

__kumar_fault-tolerant_2019__, "Fault-Tolerant Distributed Services in Message-Passing Systems":  
Pas spécifiquement à propos des fautes byzantines dans la cohérence faible mais fait un panorama des differentes fautes non-byzantine possibles dans les systèmes distribués.


__singh_zeno_2009__, "Zeno: Eventually Consistent Byzantine-Fault Tolerance":  
Algorithme pour de la convergence BFT. (Reflexions sur des erreurs byzanties possible + algo et implé)

__tseng_distributed_2019__, "Algo BFT pour cohérence causale (preuve + experiences)"

__misra_byzantine_2021__, "Preuve d'impossibilité de BFT dans un certain contexte pour de la cohérence causale + 2 algo pour de la cohérence causale BFT" 