# Script présentation Cohérence Faible

## Plan

1. Présenter un processus séquentiel classique
   - exemple : processeur monocœur
2. Introduire le concept de cohérence via la cohérence forte (le plus intuitif)
   - exemple : processeur multicœur, application distribuée centralisée.
   - notions : respect de l'ordre, atomicité, isolation
3. Introduire le concept de cohérence faible
   - exemple : application distribuée décentralisée
4. Définir les propriétés d'un système réparti
5. Définir les différents modèles de cohérence faible (des plus trivial aux moins)
   1. Cohérence Séquentielle (SC)
   2. Linéarisabilité -> Serialisabilité
   3. Convergence/Convergence Forte
      1. Définit le concept de convergence
      2. Pourquoi ? + les apports de la convergence forte
      3. Types de données basés sur la convergence (pourquoi ?)
   4. Cohérence Pipeline
      1. On présente la notion d'Intention
      2. On l'oppose à la cohérence Pipeline

6. Cohérence d'écriture
   1. Ce que ne couvre pas les modèles précédents
   2. Cohérence d'écriture et cohérence d'écriture forte.

