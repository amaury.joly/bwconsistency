
<link rel="stylesheet" href="./sunblind.css">
<!-- <link rel="stylesheet" href="./box.css"> -->

# "Event Horizon": Lot 2 21/06/2023
### JOLY Amaury (Laboratoire d'Informatique et Système, Parsec)
### __Encadrants:__ Emmanuel GODARD, Corentin TRAVERS (Equipe Algorithmique Distribuée)

---

# Rappel des objectifs

--

## Pour Parsec

__Lot 2 :__ Ajout à Parsec d'un outil d'édition collaborative.

Dans approche visant à maximiser les performances (latence et interactivité) et la résilience.

--

## Pour le LIS

Produire de la recherche sur la cohérence faible en milieu byzantin.

--

## La synthèse des deux

1. Faire un état de l'art des solutions existantes.
    - Prendre des décisions sur les choix techniques à adopter.
2. Recherche d'un compromis algorithmique __résilience__/__intéractivité__.
3. Réaliser un produit innovant dans sa gestion de la cohérence.
    - Chercher une valeur ajoutée pour l'utilisateur.

---

# Mon travail depuis avril
## Quelques définitions

--

## Quelques définitions

__Systèmes répartis :__  
Système composé d'un ensemble d'acteur interconnecté réalisant une tâche commune.

<img src="./img/reparti.png" width="400"/>

--

## Quelques définitions

__Cohérence dans un système reparti:__  
Étude du comportement des données dans un système reparti d'un point de vue observateur.

__Cohérence forte :__  
Comportement attendu dans le cas d'une exécution à un seul acteur.

---

# Mon travail depuis avril
## État de l'art


--

## État de l'art

- Cohérence dans les systèmes répartis.
  - Lamports (1970)
  - Perrin : Concurrence et Cohérence des Systèmes répartis (2017)
  
---
<img src="./img/perrin.png" alt="couverture \'Consistence et Cohérence des Systèmes réparties' M. Perrin" width="200"/>


--

## État de l'art

### Un (très) rapide résumé

<img src="./img/cartographie_simplifie.png" alt="cartographie des critères de cohérences" height="250"/>

- Les critères de cohérences s'articulent autour de 3 propriétés élémentaires.
- La conjonction des 3 représente le comportement d'un programme séquentiel.
- Nous cherchons un compromis pour maximiser l'interactivité du produit


--

## État de l'art

### Un (très) rapide résumé

- Inventaire des différentes solutions existantes.
  - Peu de littérature.
  - Pas de taxonomie consensuelle, travail de classification 

- Peu de réflexions sur les potentiels fautes byzantines induites par la (les ?) cohérences faibles.

---

# La suite

--

## La liste des tâches

- [ ] Réaliser l'état de l'art
  - [x] Gestion de la cohérence dans les systèmes répartis.
  - [ ] Faire l'inventaire des solutions existantes. (BFT weak consistency).
    - [x] Lister les solutions algorithmiques dans la littérature.
    - [ ] S'intéresser aux solutions open-source (e.g.: etherpad)
    - [ ] Classer les solutions.
- [ ] Produire/adapter un algorithme dans une situation de référence (à déterminer).
    - [ ] Formaliser les besoins du produit.
    - [ ] Essayer de les satisfaire.
- [ ] Réaliser un PoC.


--

## Pour les mois à venir

Je vous propose une réunion en distanciel (~2h) pour :
1. Vous présenter l'état de l'art.
2. Placer le produit suivant ces contraintes.

---

# Merci !
<img src="./img/cartographie.png" alt="cartographie des critères de cohérences" height="450"/>
